import $ from "jquery";
// import 'select2';
// import 'select2/dist/css/select2.css';
import '@fortawesome/fontawesome-pro/js/all.js';

// import components
import OpenMenu from './components/openMenu';
import SlideDownMainMenu from './components/slideDownMainMenu';
import SlideDownMenuUser from './components/slideDownMenuUser';
import MainMenuSubmenus from './components/mainMenuSubmenus';
import CloseOnOverlayClick from './components/closeOnOverlayClick';

const components = [
    {
        class: OpenMenu,
        selector: '.js-open-main-menu'
    },
    {
        class: SlideDownMainMenu,
        selector: '.js-all-categories',
        options: {
            target: '.menu--first-level'
        }
    },
    {
        class: SlideDownMenuUser,
        selector: '.js-show-more-user',
        options: {
            target: '.drop-down--user'
        }
    },
    {
        class: MainMenuSubmenus,
        selector: '.menu__li--have-second-level',
    },
    {
        class: CloseOnOverlayClick,
        selector: '.overlay',
        options: {
            target: ['html', 'aside']
        }
    }
];

components.forEach(component =>{
    if (document.querySelector(component.selector) !== null)
    {
        document.querySelectorAll(component.selector).forEach(
            element => new component.class(element, component.options)
        );
    }
});



let windowWidth = $(window).width();

if ( windowWidth < 992 )
{
    console.log('Manje je 992px');

    // click on first level
    $('.menu__li.menu__li--first-level.menu__li--have-second-level').click(function(e) {
        e.preventDefault();
        console.log(this);

        let closestSubmenuBlock = $(this).find('.second-level-menu-container');
        closestSubmenuBlock.addClass('second-level-menu-container--open');
    });

    // click on second level
    $('.second-level-menu-container .second-level-lists .menu--second-level').click(function(e) {
        e.preventDefault();
        let closestSubmenuBlock2 = $(this).next('.menu--third-level');
        closestSubmenuBlock2.addClass('menu--third-level--mobile-open');
    });

    // back on first level
    $('.js-back-to-second').click(function(e) {
        e.preventDefault();
        console.log(this);

        let closestThrdLevel = $(this).closest('.menu--third-level');
        closestThrdLevel.removeClass('menu--third-level--mobile-open');
    });

    // back on second level
    $('.js-back-to-first').click(function(e) {
        e.preventDefault();
        console.log(this);

        let closestSecondLevel = $(this).closest('.second-level-menu-container');
        closestSecondLevel.addClass('second-level-menu-container--open2');
    });
}