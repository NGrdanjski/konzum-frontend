export default class openMenu {

    constructor(element) {
        //console.log('aaa');
        const htmlElement = document.querySelector("html");
        const trigger = document.querySelector(".js-open-main-menu");
        const target = document.querySelector('.aside--main-menu');

        element.addEventListener('click', () => {
            this.showMenu(target, trigger, htmlElement)
        });
    }

    showMenu(target, trigger, htmlElement){
        target.classList.toggle('aside--main-menu--open');
        trigger.classList.toggle('aside--main-menu--open');
        htmlElement.classList.toggle('main-menu--open');
    }

}