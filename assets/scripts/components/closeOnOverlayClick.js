import $ from "jquery";

export default class closeOnOverlayClick {

    constructor(element, component) {

        // events
        element.addEventListener('click', () => {
            this.closeAll(component)
        });
    }


    closeAll(component){
        component.target.forEach( element =>{

            if ( $(element).hasClass('main-menu--open') || $(element).hasClass('aside--main-menu--open') )
            {
                $(element).removeClass('main-menu--open');
                $(element).removeClass('aside--main-menu--open');
            }
        });
    }
}