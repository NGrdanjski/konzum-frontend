import $ from "jquery";

export default class slideDownMainMenu {

    constructor(element, component) {
        let visibleItems = $(component.target).attr('data-li-visible');
        let item = $('.menu--first-level .menu__li')[0];
        let items = $('.menu--first-level .menu__li');
        let itemHeight = $(item).height();
        let fullMenuHeight = null;
        let menu = $(component.target);
        let menuOffset = menu.offset();
        let setHeight = itemHeight * visibleItems;
        let windowHeight = window.innerHeight;
        let menuFromTop = null;

        //console.log(windowHeight);

        // set menu height based on number visible items
        menu.css('height', setHeight);
        menu.css('overflow', 'hidden');

        // events
        element.addEventListener('click', () => {
            this.showAll(menu, items, fullMenuHeight, menuOffset, windowHeight)
        });
    }


    showAll(menu, items, fullMenuHeight, menuOffset, windowHeight){
        $(items).each(function() {
            fullMenuHeight += $(this).height();
        });
        let calc = windowHeight - menuOffset.top;
        //console.log(cca);
        menu.css('height', 1150);
        $('.label--all-categories').text('Pogledaj manje...');
        //menu.css('overflow', 'unset');
        //menu.css('overflow-x', 'scroll');
        //menu.css('overflow-y', 'hidden');
    }
}