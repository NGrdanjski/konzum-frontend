import $ from "jquery";

export default class mainMenuSubmenus {

    constructor(element) {

        let windowWidth = $(window).width();

        // hover
        $(element).hover(function(){

            // window height
            let windowHeight = $(window).height();

            // closest second level block
            let closestSubmenuBlock = $(this).find('.second-level-menu-container');

            // closest submenu height
            let SubmenuBlockHeight = $(closestSubmenuBlock).height();

            // submenu block offset
            let submenuOffset = $(this).offset();

            // submenu offset from top
            let submenuOffsetTop = submenuOffset.top - SubmenuBlockHeight / 2;

            // submenu offset from bottom
            let submenuOffsetBottom = windowHeight - SubmenuBlockHeight - submenuOffsetTop - 45;

            // convert minus bottom value to positive value
            let submenuOffsetBottomPositiveValue =- submenuOffsetBottom;

            // check if is submenu block touch bottom
            if ( submenuOffsetBottom <= 0 )
            {
                $('.second-level-menu-container').css('top', submenuOffsetTop - submenuOffsetBottomPositiveValue);
            }
            else
            {
                $('.second-level-menu-container').css('top', submenuOffsetTop);
            }
        });


        // belove 992px
        // if ( windowWidth < 992 )
        // {
        //     console.log('992');
        //     // on click event
        //     $(element).click(function(e) {
        //         e.preventDefault();
        //         this.showMenu()
        //     });
        // }
    }

    // showMenu(){
    //     console.log('aaaaaa');
    // }
}