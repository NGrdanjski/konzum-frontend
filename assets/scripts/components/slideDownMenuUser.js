import $ from "jquery";

export default class slideDownMenuUser {

    constructor(element, component) {

        let dropDownWrapper = $(component.target);
        let item = $('.drop-down--user ul li')[0];
        let items = $('.drop-down--user ul li');
        let itemHeight = $(item).height();
        let fullMenuHeight = null;
        let isOpen = false;

        $(items).each(function() {
            fullMenuHeight += $(this).height();
        });

        // events
        element.addEventListener('click', () => {
            this.showAll(fullMenuHeight, dropDownWrapper)
        });
    }


    showAll(fullMenuHeight, dropDownWrapper){

        dropDownWrapper.toggleClass('drop-down--open');

        if ( !$(dropDownWrapper).hasClass('drop-down--open') )
        {
            dropDownWrapper.css('height', 0);
        }
        else
        {
            dropDownWrapper.css('height', fullMenuHeight + 15);
        }
    }
}