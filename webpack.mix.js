let mix = require('laravel-mix');

mix.js('assets/scripts/app.js', 'dist/scripts')
    .sass('assets/styles/app.scss', 'dist/styles')
    .setPublicPath('dist');